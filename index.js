// Load the expressjs module into our application and saved it in a varaible called express
// require ("package")
const express = require("express");

// localhost port number
const port = 4000;

// app is our server
const app = express();

// middleware
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming JSON from our request.body
app.use(express.json());

// mock data
let users = [
	{
		username: "TStark3000",
		email: "starksindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},
	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
];

// Express has methods to use as routes corresponding to HTTP methods
// Syntax: app.method(<endpoint>, <function for request and response>);

app.get('/', (request, respond) => {
	// respond.send() actually combines writeHead() and end()
	respond.send('Hello from my first ExpressJS API');
	// respond.status(200).send("message");
});


app.get('/greeting', (request, respond) => {

	respond.send('Hello from Batch203-surname');
});

//retrieval of the users in mock database
app.get('/users', (request,respond) => {
	// res.send alredy stringifies for you
	respond.send(users);
});

// adding of new user pag meron ibbigay na document through the request body
app.post('/users', (request, respond) =>{

	//console.log(request.body) // result: {} empty object

	// simulate creating a new user document
	let newUser = {
		username: request.body.username,
		email:request.body.email,
		password:request.body.password
	};

	// adding newUser to the existing array
	users.push(newUser);
	console.log(users);

	//send the updated users array in the client
	respond.send(users);

})

// DELETE Method
app.delete('/users', (request,respond) =>{
	// deleting the last user of the users array
	users.pop();
	respond.send(users);
});

//PUT Method
// update user's password
// :index - wildcard
//url: localhost:4000/users/0
app.put('/users/:index', (request, respond)=>{

	console.log(request.body); // updated password
	// an object that contains the value of URL params
	console.log(request.params); // refer to the wildcard in the endpoint
	//result: {index:0}

	// parseInt the value of the number coming from req.params
	// ['0'] turns into [0]
	let index = parseInt(request.params.index)
	//users[0].password
		//"notPeterParker" = "tonyStark"
		// the update will be coming from the request body
	users[index].password = request.body.password;
	// send the updated user
	respond.send(users[index]);
})

/*
	Activity : 30 mins

	    Endpoint: /items


	    >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
	        >> pass the index number of the item that you want to update in the request params
	        >> add the price update in the request body
	        >> reassign the new price from our request body
	        >> send the updated item in the client

	    >> Create a new collection in Postman called s34-activity
	    >> Save the Postman collection in your s34 folder
*/

app.get('/items', (request,respond) => {

	respond.send(items);
});

app.post('/items', (request, respond) =>{

	let newItem = {
		name: request.body.name,
		price:request.body.price,
		isActive:request.body.isActive
	};

	items.push(newItem);
	console.log(items);

	respond.send(items);

});

app.put('/items/:index', (request, respond)=>{

	console.log(request.body); 

	console.log(request.params); 

	let index = parseInt(request.params.index)
	
	items[index].price = request.body.price;
	respond.send(items[index]);
});


// port listener
app.listen(port, () => console.log(`Server is running at port ${port}`));